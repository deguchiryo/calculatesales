package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		// コマンドライン引数が渡されてなかった場合のエラー処理
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売り上げ金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", "支店")) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[A-Za-z0-9]{8}$", "商品")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();

		// rcdファイルを保持する為のList
		ArrayList<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {
			String file = files[i].getName();

			if(files[i].isFile() && file.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}

		}

		// rcdFilesリストを昇順に並び変える
		Collections.sort(rcdFiles);

		// rcdファイル名が連番でなかった場合のエラー処理
		for(int i = 0; i < rcdFiles.size() -1; i++) {
			int format = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - format) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for(int i = 0; i < rcdFiles.size(); i ++) {
			BufferedReader br = null;

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				// 売り上げファイルの中身を保持する為のList
				ArrayList<String> salesValue = new ArrayList<>();

				String line = null;

				while((line = br.readLine()) != null) {
					salesValue.add(line);
				}

				// 売り上げファイルが3行でなかった場合のエラー処理
				if(salesValue.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				// 売り上げファイルの支店コードが支店定義ファイルになかった時のエラー処理
				if(!branchSales.containsKey(salesValue.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				// 売り上げファイルの商品コードが商品定義ファイルになかった時のエラー処理
				if(!commoditySales.containsKey(salesValue.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

				// 売り上げファイルの売り上げ金額が数字でなかった場合のエラー処理
				if(!salesValue.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				Long fileSales = Long.parseLong(salesValue.get(2));

				// 支店別売り上げ合計金額
				Long saleAmount = branchSales.get(salesValue.get(0)) + fileSales;

				// 商品別売り上げ合計金額
				Long commoditySaleAmonut = commoditySales.get(salesValue.get(1)) + fileSales;

				// 売り上げ金額の合計が10桁超えた時のエラー処理
				if(saleAmount >= 10000000000L || commoditySaleAmonut >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put(salesValue.get(0), saleAmount);
				commoditySales.put(salesValue.get(1), commoditySaleAmonut);
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードor商品コードと支店名or商品名を保持するMap
	 * @param 支店コードor商品コードと売上金額を保持するMap
	 * @param 正規表現
	 * @param エラー処理に用いるファイル名
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String format, String errorFileName) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			// ファイルが存在しなかった場合のエラー処理
			if(!file.exists()) {
				System.out.println(errorFileName + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line = null;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				// 定義ファイルのフォーマットが不正だった場合のエラー処理
				if((items.length) != 2  || (!items[0].matches(format))) {
					System.out.println(errorFileName + FILE_INVALID_FORMAT);
					return false;
				}

				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}


		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードor商品コードと支店名or商品名を保持するMap
	 * @param 支店コードor商品コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter  fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : sales.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
